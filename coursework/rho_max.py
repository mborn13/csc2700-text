#!/usr/bin/env python

# Plot evolution of central density
#import matplotlib
#matplotlib.use('Agg')
#import matplotlib.pyplot as plti
from plot_defaults import *

# load data
Fx, Fy = numpy.loadtxt("dx8.asc.bz2",
                       comments="#", usecols=(1,2), unpack=True)

Gx, Gy = numpy.loadtxt("dx4.asc.bz2",
                       comments="#", usecols=(1,2), unpack=True)
# plot basics
fig = plt.figure(figsize=(6,3))
fig.subplots_adjust(top=0.85, bottom=0.16, left=0.11,right=0.97)
ax = fig.add_subplot(1,1,1)

# plot
ax.plot(Gx/200, 100*Gy/Gy[0], linestyle='--', color='blue')
ax.plot(Fx/200, 100*Fy/Fy[0], linestyle='-', color='black')

ax.set_xlabel(r't [ms]')
ax.set_xlim(0,6.5)
ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
ax.xaxis.grid(False)
ax2 = ax.twiny()
ax2.set_xlabel(r't [M]')
ax2.xaxis.set_minor_locator(mticker.AutoMinorLocator())
ax2.set_xlim(0,1300)
ax.set_ylabel(r'$\varrho_c/\varrho_c(0)$ [%]')
ax.yaxis.set_major_locator(mticker.MaxNLocator(4))
ax.yaxis.set_minor_locator(mticker.AutoMinorLocator())
ax.yaxis.grid(False)
ax.set_ylim(99.0,100.5)
set_tick_sizes(ax, 8, 4)
ax.legend(('dx = 4', 'dx = 8'), 'upper left')

#plt.show()
plt.savefig('rho_max.pdf')

